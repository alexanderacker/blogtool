use std::{
    fs::File,
    io::{self, Read},
    path::PathBuf,
};

use serde_derive::{Deserialize, Serialize};
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct RawManifest {
    pub name: String,
    pub author: String,
    //pub pathinfo: PathInfo,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct PathInfo {
    pub out_dir: Option<String>,
    pub src_dir: Option<String>,
}

impl RawManifest {
    pub fn from_file(file: PathBuf) -> Result<Self, io::Error> {
        let mut manifest = File::open(file)?;
        let metadata = manifest.metadata()?;

        let reported_len = metadata.len() as usize;
        let mut contents = String::with_capacity(reported_len);
        let len = manifest.read_to_string(&mut contents)?;
        if len != reported_len {
            return Err(io::Error::new(
                std::io::ErrorKind::InvalidData,
                "Could not read the entire file length!",
            ));
        }

        Ok(toml::from_str(&contents)?)
    }
}
