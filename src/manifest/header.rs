use serde_derive::{Deserialize, Serialize};
use toml::value::Datetime;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct Header {
    pub title: String,
    pub creation_date: Option<Datetime>,
    pub authors: Option<Vec<String>>,
    pub tags: Option<Vec<String>>,
    pub series: Option<Series>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct Series {
    pub topic: String,
    pub index: i64,
}
