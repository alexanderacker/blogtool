use std::fmt;

pub mod md;

#[derive(Clone, Debug)]
pub struct ProcessingError(String);

impl fmt::Display for ProcessingError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error processing file: {}", self.0)
    }
}

pub trait InputFile<Header> {
    fn to_html(&mut self) -> Result<(String, Header), ProcessingError>;
}

// need some way to map input files to
