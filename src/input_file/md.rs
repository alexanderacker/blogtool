use comrak::{
    arena_tree::Node,
    format_html,
    nodes::{Ast, NodeValue},
    parse_document, Arena, ComrakOptions,
};

use crate::manifest::header::Header;

use super::{InputFile, ProcessingError};
use std::{
    cell::RefCell,
    fs::File,
    io::{self, Read},
    path::PathBuf,
};

pub struct Md<'a> {
    _file: PathBuf,
    root: &'a Node<'a, RefCell<Ast>>,
}

impl<'a> Md<'a> {
    pub fn from_file(
        file: PathBuf,
        arena: &'a Arena<Node<'a, RefCell<Ast>>>,
    ) -> Result<Self, io::Error> {
        let mut md = File::open(file.clone())?;
        let metadata = md.metadata()?;

        let reported_len = metadata.len() as usize;
        let mut contents = String::with_capacity(reported_len);
        let len = md.read_to_string(&mut contents)?;
        if len != reported_len {
            return Err(io::Error::new(
                std::io::ErrorKind::InvalidData,
                "Could not read the entire file length!",
            ));
        }
        let root = parse_document(arena, &contents, &ComrakOptions::default());
        Ok(Self { _file: file, root })
    }
}

impl<'a> InputFile<Header> for Md<'a> {
    fn to_html(&mut self) -> Result<(String, Header), ProcessingError> {
        fn get_header_data<'a>(md: &'a mut Md) -> Result<Header, ProcessingError> {
            if let Some(node_ref) = md.root.children().peekable().peek() {
                if let NodeValue::CodeBlock(block) = &node_ref.data.borrow_mut().value {
                    if block.fenced == true && block.fence_length == 3 {
                        if std::str::from_utf8(&block.info).unwrap() == "toml" {
                            let strin = std::str::from_utf8(&block.literal).unwrap();
                            let header: Header = match toml::from_str(strin) {
                                Ok(h) => h,
                                Err(_e) => {
                                    return Err(ProcessingError(
                                        "Header does not follow specification".to_string(),
                                    ));
                                }
                            };

                            md.root.first_child().unwrap().detach(); // prevents block from being rendered in html
                            Ok(header)
                        } else {
                            return Err(ProcessingError(
                                "Markdown header is not of type 'toml'".to_string(),
                            ));
                        }
                    } else {
                        return Err(ProcessingError(
                            "Markdown header is not fenced or fence is not 3 long".to_string(),
                        ));
                    }
                } else {
                    return Err(ProcessingError(
                        "First item in Markdown file was not a header!".to_string(),
                    ));
                }
            } else {
                return Err(ProcessingError(
                    "Markdown file is missing a header!".to_string(),
                ));
            }
        }

        let header = get_header_data(self)?;
        let mut html = Vec::default();
        format_html(self.root, &ComrakOptions::default(), &mut html).unwrap();

        let html_string = String::from_utf8(html).unwrap();
        Ok((html_string, header))
    }
}
