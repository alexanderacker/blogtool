use clap::{Parser, Subcommand};
use command::{
    build::{build, BuildArgs},
    new::{new, NewArgs},
};
use std::env;

mod command;
mod input_file;
mod manifest;

#[derive(Parser)]
#[clap(name = "sitegen", about = "Static site generator", long_about = None)]
struct SiteGenArgs {
    #[clap(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
enum Command {
    New(NewArgs),
    Build(BuildArgs),
}

fn main() {
    env_logger::init();
    let cwd = env::current_dir().expect("Could not get the current directory!");

    let args = SiteGenArgs::parse();

    match args.command {
        Command::New(args) => {
            new(cwd, args);
        }
        Command::Build(args) => {
            build(cwd, args);
        }
    }
}
