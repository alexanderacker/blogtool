use std::{
    fs::{read_dir, write},
    path::PathBuf,
    process::exit,
};

use clap::Args;
use comrak::Arena;
use log::error;
use normpath::PathExt;
use tera::Tera;

use crate::{
    input_file::{md::Md, InputFile},
    manifest::manifest::RawManifest,
};

#[derive(Args, Default, Debug)]
pub struct BuildArgs {
    #[clap(long)]
    dry_run: bool,
    #[clap(long, default_value = "Site.toml", value_hint = clap::ValueHint::FilePath)]
    manifest_path: std::path::PathBuf,
}
/*
    1) Make the html go in some output directory
    2) Factor out markdown functions to another file
    3) Parse the toml header and do something useful with it (get some tags)
    4) Abstract out the context stuff so that markdown can interface with other template engines (if simple)
*/
pub fn build(cwd: PathBuf, args: BuildArgs) {
    // Get the manifest path or default to ./Site.toml if none is provided
    // Some of this logic is lifted from cargo
    // For later: src/cargo/util/toml is probably useful for parsing.
    let manifest_path_norm = match cwd.join(args.manifest_path).normalize() {
        Ok(path) => {
            if !path.ends_with("Site.toml") {
                error!("the manifest path must be a path to a Site.toml file");
                exit(-1);
            }
            path.into_path_buf()
        }
        Err(e) => {
            error!("1. Error normalizing manifest path: {}", e.to_string());
            exit(-1);
        }
    };

    let mut root_dir = manifest_path_norm.clone();
    root_dir.pop();

    if !manifest_path_norm.exists() {
        error!(
            "the manifest path `{}` does not exist",
            manifest_path_norm.into_os_string().into_string().unwrap()
        );
        exit(-1);
    }

    let manifest: RawManifest = match RawManifest::from_file(manifest_path_norm) {
        Ok(x) => x,
        Err(e) => {
            error!("Error reading manifest: {}", e.to_string());
            exit(-1);
        }
    };

    println!("Name: {}, Author: {}", manifest.name, manifest.author);

    // nice, so we can find and read the manifest toml!

    // find the source directory
    let src_dir = match root_dir
        .join(PathBuf::from("src"))
        .normalize()
        .map(|path| path.into_path_buf())
    {
        Ok(src) => {
            if !src.is_dir() {
                error!("No src directory!");
                exit(-1);
            }
            src
        }
        Err(e) => {
            error!("2. Error normalizing manifest path: {}", e.to_string());
            exit(-1);
        }
    };

    // get all markdown files in the src directory
    let src_dir_files = match read_dir(&src_dir) {
        Ok(read_dir) => read_dir.filter(|file| {
            if let Ok(direntry) = file {
                return match direntry.path().extension() {
                    Some(extension) => extension.to_ascii_lowercase() == "md",
                    None => false,
                };
            }
            false
        }),
        Err(err) => {
            error!("Failed to find files in the src directory!");
            println!("Error(s): {}", err.to_string());
            exit(-1);
        }
    };

    let templates: Tera = {
        match root_dir
            .join(PathBuf::from("template"))
            .normalize()
            .map(|path| path.into_path_buf())
        {
            Ok(template) => {
                if template.is_dir() {
                    let mut path = template
                        .to_path_buf()
                        .into_os_string()
                        .to_str()
                        .unwrap()
                        .to_string();
                    path.push(std::path::MAIN_SEPARATOR);
                    path.push('*');
                    let res = Tera::new(path.as_str());
                    if let Err(e) = res {
                        error!("Template parsing errors!");
                        println!("Error(s): {}", e);
                        exit(-1);
                    }
                    let mut tera = res.unwrap();
                    tera.autoescape_on(vec![]);
                    tera
                } else {
                    error!("Failed to find the template directory!");
                    exit(-1);
                }
            }
            Err(e) => {
                error!("3. Error normalizing manifest path: {}", e.to_string());
                exit(-1);
            }
        }
    };

    println!("Templates:");
    for name in templates.get_template_names() {
        println!("  {}", name);
    }

    // should probably assert that we have the correct templates here.
    let mut context = tera::Context::new();
    context.insert("manifest", &manifest);
    //context.insert("name", &manifest.name);
    //context.insert("author", &manifest.author);

    // for each file in the source directory do something
    let arena = Arena::new();
    for res in src_dir_files {
        match res {
            Ok(entry) => {
                if !entry.path().is_dir() {
                    println!(
                        "Found file: {}",
                        entry
                            .path()
                            .to_path_buf()
                            .into_os_string()
                            .to_str()
                            .unwrap()
                    );

                    let mut md = match Md::from_file(entry.path(), &arena) {
                        Ok(md) => md,
                        Err(e) => {
                            error!(
                                "Error reading markdown: {} from file: {}",
                                e.to_string(),
                                entry
                                    .path()
                                    .to_path_buf()
                                    .into_os_string()
                                    .to_str()
                                    .unwrap()
                            );
                            exit(-1);
                        }
                    };
                    let (markdown_html, _header) = match md.to_html() {
                        Ok(res) => res,
                        Err(e) => {
                            error!(
                                "Error converting markdown to html: {} from file: {}",
                                e.to_string(),
                                entry
                                    .path()
                                    .to_path_buf()
                                    .into_os_string()
                                    .to_str()
                                    .unwrap()
                            );
                            exit(-1);
                        }
                    };
                    // update the context
                    context.insert("post", &markdown_html);
                    let template_file = String::from("template.html");

                    let text = match templates.render(template_file.as_str(), &context) {
                        Ok(page) => page,
                        Err(e) => {
                            error!("Failed to render template: {}!", template_file.as_str());
                            println!("Error(s): {}", e);
                            exit(-1);
                        }
                    };

                    // save the output in the same directory with a different extension
                    if !args.dry_run {
                        let out = entry.path().with_extension("").with_extension("html");
                        write(out, text).expect("Unable to write file");
                    }
                }
            }
            Err(err) => {
                error!(
                    "Failed to gather information on file.\n Error: {}",
                    err.to_string()
                );
            }
        }
    }

    // get all the files inside

    // next steps:
    /*
       scan directory 'src' for markdown files
       scan directory 'template' for html and css templates

       open each markdown file 'src/FILE.md':
           read file info from an initial toml block (including template it uses)
           convert markdown to html
           insert data into a fresh template
           save the template as out/FILE.html
    */

    /* Targets:
        - Cargo automatically searches for src/lib.rs, src/main.rs
        - OR a 'target table' in the toml: [lib] or [[bin]]

        For now just scan some folder automagically, this should probably be a function.
        Want two types:
            - single layer scan and recursive scan of files.
            - do I want to force the user to manually include blog entries in an index file? (NOT REALLY, IT'S ANNOYING)
            - or should I make it opt-out per file?


        Should all files go into source? It would be more convenient if NOT so
        1) 'src' directory: markdown (or other kind of input) files
        2) 'template' directory: html and sass/css templates, FONTS?
        3) 'res' directory: store images and other files here
            - Should I sort res into folders by content type? eg: 'image' 'audio' 'video' 'font' at the top-level
            - project should be able to reference things in res
            - using both relative path
            - and root path where root is assumed to be 'res' itself.
    */

    /*
        TOML Guide: https://toml.io/en/v1.0.0
        Maybe I could try parsing the Manifest
    */

    /*
    if let Some(o) = matches.value_of("output") {
        println!("Value for output: {}", o);
    }

    if let Some(c) = matches.value_of("config") {
        println!("Value for config: {}", c);
    }

    // You can see how many times a particular flag or argument occurred
    // Note, only flags can have multiple occurrences
    match matches.occurrences_of("debug") {
        0 => println!("Debug mode is off"),
        1 => println!("Debug mode is kind of on"),
        2 => println!("Debug mode is on"),
        _ => println!("Don't be crazy"),
    }

    // You can check for the existence of subcommands, and if found use their
    // matches just as you would the top level app
    if let Some(matches) = matches.subcommand_matches("test") {
        // "$ myapp test" was run
        if matches.is_present("list") {
            // "$ myapp test -l" was run
            println!("Printing testing lists...");
        } else {
            println!("Not printing testing lists...");
        }
    }*/
}
