use std::{
    fs::{create_dir, OpenOptions},
    path::PathBuf,
    process::exit,
};

use clap::Args;
use log::error;

#[derive(Args, Default, Debug)]
pub struct NewArgs {
    name: String,
}

pub fn new(cwd: PathBuf, args: NewArgs) {
    let subdirs = vec!["static", "src", "template"];
    let base_directory = cwd.join(&args.name);
    if base_directory.exists() {
        error!(
            "The base directory '{}' already exists in '{}'",
            args.name,
            cwd.as_os_str().to_str().unwrap(),
        );
        exit(-1);
    }
    if let Err(e) = create_dir(&base_directory) {
        error!(
            "Failed to create base directory '{}' in '{}'. Error: {}",
            base_directory.as_os_str().to_str().unwrap(),
            cwd.as_os_str().to_str().unwrap(),
            e.to_string()
        );
        exit(-1);
    }
    for dir in subdirs {
        if let Err(e) = create_dir(base_directory.join(dir)) {
            error!(
                "Failed to create subdirectory '{}' in '{}'. Error: {}",
                dir,
                base_directory.as_os_str().to_str().unwrap(),
                e.to_string()
            );
            exit(-1);
        }
    }

    if let Err(e) = OpenOptions::new()
        .create(true)
        .write(true)
        .open(base_directory.join("Site.toml"))
    {
        error!(
            "Failed to create '{}' in '{}'. Error: {}",
            "Site.toml".to_string(),
            base_directory.as_os_str().to_str().unwrap(),
            e.to_string()
        );
        exit(-1);
    }
}
