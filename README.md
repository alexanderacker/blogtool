# sitegen
- Proof-of-concept static-site generator
- Markdown -> HTML Webpages
- Used to assist in building a static-site without needing to copy/paste code and update multiple files
- Similar in concept to other engines like Hugo

## Command-line arguments
### new
create a new blank project
``sitegen new <PROJECT-NAME>``

### build
build a project
``sitegen build <PROJECT_NAME>``
produces one html output file per Markdown file

## Example site "test"
Create a new directory called 'test' with the following files
- Configuration TOML :: test/Site.toml
- Markdown pages :: test/src/*.md
- HTML template :: test/template/template.html

Then build in the directory of Site.toml and it will produce one html file per md file.

## Note
Project is completely a proof of concept and is not intended to be used.
However with some polish it could be made into a usable static-site generator.
